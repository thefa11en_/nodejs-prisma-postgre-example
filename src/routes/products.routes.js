import { Router } from 'express';
import { prisma } from '../db.js';

const router = Router();

router.get('/products', async (req, res) => {
  const products = await prisma.product.findMany();
  res.json(products);
});

router.get('/products/:id', async (req, res) => {
  const product = await prisma.product.findFirst({
    where: {
      id: parseInt(req.params.id),
    },
    include: {
      category: {
        select: {
          name: true,
        },
      },
    },
  });

  //product.category = product.category.name;
  
  
  res.json(product);
});

router.post('/products', async (req, res) => {
  const newProduct = await prisma.product.create({
    data: req.body,
  });

  res.json(newProduct);
});

router.delete('/products/:id', async (req, res) => {
  const productDeleted = await prisma.product.delete({
    where: {
      id: parseInt(req.params.id),
    },
  });

  res.json(productDeleted);
});

router.put('/products/:id', async (req, res) => {
  const productUpdate = await prisma.product.update({
    where: {
      id: parseInt(req.params.id),
    },
    data: req.body,
  });

  return res.json(productUpdate);
});

export default router;
